﻿using System;
using System.Collections.Generic;

namespace Models
{
    [Serializable]
    public struct HighScores
    {
        public List<HighScore> scores;
    }

    [Serializable]
    public struct HighScore
    {
        public string level;

        public int score;
    }
}