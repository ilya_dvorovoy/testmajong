﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : Singleton<LevelLoader>
{
    private TextAsset level;
    public string LoadedLevel => level.text;
    public string LevelName => level.name;

    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(this);
    }

    public void LoadLevel(TextAsset level)
    {
        SceneManager.LoadScene("Gameplay");
        this.level = level;
    }

    public void LoadTitle()
    {
        SceneManager.LoadScene("Title");
        this.level = null;
    }

}
