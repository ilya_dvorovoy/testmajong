﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Utilities;
using Random = System.Random;

namespace Gameplay
{
    public class TileMap
    {
        private const char TILE_CHAR = 'X';

        public int Rows => rows;
        public int Columns => cols;

        public IEnumerable<Tile> Tiles
        {
            get
            {
                foreach (var set in tiles.Values)
                {
                    for (int i = 0; i < set.Count; i++)
                    {
                        yield return set[i];
                    }
                }
            }
        }

        private int tilesCount;
        public int TilesCount => tilesCount;

        private int cols, rows;
        private Tile[,] tileMap;
        private Dictionary<TileType, List<Tile>> tiles;

        public TileMap(string level)
        {
            var map = ParseLevel(level, out tilesCount);
            if (map.Count <= 0 || tilesCount % 2 != 0)
            {
                throw new ArgumentException("Failed loading level data.");
            }

            // +2 to have the border paths available
            cols = map[0].Length + 2;
            rows = map.Count + 2;

            tileMap = RandomizeTileMap(tilesCount, map);
        }

        public void RemoveTiles(params Tile[] tiles)
        {
            foreach (var tile in tiles)
            {
                tileMap[tile.Coordinates.x, tile.Coordinates.y] = default;
                this.tiles[tile.Type].Remove(tile);

                tilesCount--;
            }
        }

        public bool MatchTiles(Tile t1, Tile t2)
        {
            if (t1.Type != t2.Type)
                return false;

            // same line
            if (t1.Coordinates.x == t2.Coordinates.x || t1.Coordinates.y == t2.Coordinates.y)
            {
                return MatchStraight(t1.Coordinates, t2.Coordinates) || MatchSLU(t1.Coordinates, t2.Coordinates);
            }
            else
            {
                return MatchSLU(t1.Coordinates, t2.Coordinates);
            }
        }

        private bool MatchStraight(Vector2Int coord1, Vector2Int coord2)
        {
            if (coord1.x == coord2.x)
            {
                return LineLookup(coord1.x, Mathf.Min(coord1.y, coord2.y), Mathf.Max(coord1.y, coord2.y), false);
            }
            else
            {
                return LineLookup(coord1.y, Mathf.Min(coord1.x, coord2.x), Mathf.Max(coord1.x, coord2.x), true);
            }
        }

        private bool LineLookup(int line, int min, int max, bool horizontal)
        {
            for (int i = min + 1; i < max; i++)
            {
                if (tileMap[horizontal ? i : line, horizontal ? line : i] != default)
                {
                    return false;
                }
            }

            return true;
        }

        private bool MatchSLU(Vector2Int coord1, Vector2Int coord2)
        {
            var left = coord1.x < coord2.x ? coord1 : coord2;
            var right = coord1.x > coord2.x ? coord1 : coord2;

            var top = coord1.y < coord2.y ? coord1 : coord2;
            var bottom = coord1.y > coord2.y ? coord1 : coord2;

            // check horizontal S-shape
            int leftExtent = -1, rightExtent = -1;
            if (TryGetExtent(left, true, true, ref leftExtent) & TryGetExtent(right, false, true, ref rightExtent))
            {
                for (int x = rightExtent; x <= leftExtent; x++)
                {
                    if (LineLookup(x, Mathf.Min(coord1.y, coord2.y), Mathf.Max(coord1.y, coord2.y), false))
                    {
                        return true;
                    }
                }
            }

            // check vertical S-shape
            int topExtent = -1, bottomExtent = -1;
            if (TryGetExtent(top, true, false, ref topExtent) & TryGetExtent(bottom, false, false, ref bottomExtent))
            {
                for (int y = bottomExtent; y <= topExtent; y++)
                {
                    if (LineLookup(y, Mathf.Min(coord1.x, coord2.x), Mathf.Max(coord1.x, coord2.x), true))
                    {
                        return true;
                    }
                }
            }

            if (leftExtent > -1 && leftExtent >= right.x)
            {
                // check L-shape
                if (LineLookup(right.x, top.y, bottom.y, false))
                    return true;

                // check U-shape
                int rightOut = -1;
                if (TryGetExtent(right, true, true, ref rightOut))
                {
                    for (int x = Mathf.Min(leftExtent, right.x + 1); x <= Mathf.Min(leftExtent, rightOut); x++)
                    {
                        if (LineLookup(x, Mathf.Min(coord1.y, coord2.y), Mathf.Max(coord1.y, coord2.y), false))
                        {
                            return true;
                        }
                    }
                }
            }
            else if (rightExtent > -1 && rightExtent <= left.x)
            {
                // check L-shape
                if (LineLookup(left.x, top.y, bottom.y, false))
                    return true;

                // check U-shape
                int leftOut = -1;
                if (TryGetExtent(left, false, true, ref leftOut))
                {
                    for (int x = Mathf.Max(rightExtent, left.x - 1); x >= Mathf.Max(rightExtent, leftOut); x--)
                    {
                        if (LineLookup(x, Mathf.Min(coord1.y, coord2.y), Mathf.Max(coord1.y, coord2.y), false))
                        {
                            return true;
                        }
                    }
                }
            }

            if (topExtent > -1 && topExtent > bottom.y)
            {
                // check U-shape
                int bottomOut = -1;
                if (TryGetExtent(bottom, true, false, ref bottomOut))
                {
                    for (int y = Mathf.Min(topExtent, bottom.y + 1); y <= Mathf.Min(topExtent, bottomOut); y++)
                    {
                        if (LineLookup(y, Mathf.Min(coord1.x, coord2.x), Mathf.Max(coord1.x, coord2.x), true))
                        {
                            return true;
                        }
                    }
                }
            }
            else if (bottomExtent > -1 && bottomExtent < top.y)
            {
                // check U-shape
                int topOut = -1;
                if (TryGetExtent(top, false, false, ref topOut))
                {
                    for (int y = Mathf.Max(bottomExtent, bottom.y - 1); y >= Mathf.Max(bottomExtent, topOut); y--)
                    {
                        if (LineLookup(y, Mathf.Min(coord1.x, coord2.x), Mathf.Max(coord1.x, coord2.x), true))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool TryGetExtent(Vector2Int start, bool increase, bool horizontal, ref int index)
        {
            int finish = increase ? horizontal ? cols : rows : 0;
            int shift = increase ? 1 : -1;

            for (int i = (horizontal ? start.x : start.y) + shift; ; i += shift)
            {
                if (increase && i >= finish)
                    break;
                else if (!increase && i < finish)
                    break;

                if (tileMap[horizontal ? i : start.x, horizontal ? start.y : i] != default)
                {
                    return index > -1;
                }
                else
                {
                    index = i;
                }
            }

            return index > -1;
        }

        private Tile[,] RandomizeTileMap(int totalTiles, List<bool[]> map)
        {
            var tileMap = new Tile[cols, rows];

            var faces = totalTiles / 2;
            var randomSet1 = new int[faces];
            var randomSet2 = new int[faces];
            for (int i = 1; i <= faces; i++)
            {
                randomSet1[i - 1] = i;
                randomSet2[i - 1] = i;
            }

            var rng = new Random();
            rng.Shuffle(randomSet1);
            rng.Shuffle(randomSet2);

            // safeguard for faces overflow
            var maxFaces = Enum.GetValues(typeof(TileType)).Length - 1;// -1 to skip the "None" default

            tiles = new Dictionary<TileType, List<Tile>>();
            for (int row = 0; row < map.Count; row++)
            {
                for (int col = 0; col < map[row].Length; col++)
                {
                    if (map[row][col])
                    {
                        var set = totalTiles % 2 == 0 ? randomSet1 : randomSet2;
                        int face = set[faces - 1];

                        // repeat faces over defined limit
                        if (face > maxFaces)
                        {
                            face = face % maxFaces;
                        }

                        faces -= totalTiles % 2;
                        totalTiles--;

                        // +1 to offset from the border
                        var tile = new Tile()
                        {
                            Coordinates = new Vector2Int(col + 1, row + 1),
                            Type = (TileType)face
                        };
                        tileMap[col + 1, row + 1] = tile;

                        // keep track of all the tiles for easier matching
                        if (tiles.ContainsKey(tile.Type))
                        {
                            tiles[tile.Type].Add(tile);
                        }
                        else
                        {
                            tiles[tile.Type] = new List<Tile>() { tile };
                        }
                    }
                }
            }

            return tileMap;
        }

        private List<bool[]> ParseLevel(string level, out int totalTiles)
        {
            totalTiles = 0;
            var mapData = new List<bool[]>();

            using (var reader = new StringReader(level))
            {
                var line = reader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    int tilesCount;
                    mapData.Add(ParseMapLine(line, out tilesCount));

                    totalTiles += tilesCount;

                    line = reader.ReadLine();
                }
            }

            return mapData;
        }

        private bool[] ParseMapLine(string line, out int tilesCount)
        {
            tilesCount = 0;
            var tiles = new bool[line.Length];

            for (int i = 0; i < line.Length; i++)
            {
                if (line[i].Equals(TILE_CHAR))
                {
                    tilesCount++;
                    tiles[i] = true;
                }
            }

            return tiles;
        }
    }
}