﻿using UnityEngine;

namespace Gameplay
{
    public struct Tile
    {
        public Vector2Int Coordinates;
        public TileType Type;

        public static bool operator ==(Tile a, Tile b)
        {
            return a.Coordinates == b.Coordinates && a.Type == b.Type;
        }

        public static bool operator !=(Tile a, Tile b)
        {
            return a.Coordinates != b.Coordinates || a.Type != b.Type;
        }
    }
}