﻿using System;
using UI;
using UnityEngine;

namespace Gameplay
{
    public class GameplayManager : MonoBehaviour
    {
        public event Action OnGameWon;
        public event Action OnGameLost;

        public event Action OnTilesMatched;
        public event Action OnTilesMatchFailed;

        [SerializeField]
        private TileMapView tileMapView;

        private TileMap tileMap;

        private Tile currentSelection;

        private void Awake()
        {
            var map = LevelLoader.Instance.LoadedLevel;
            tileMap = new TileMap(map);

            tileMapView.Initialize(tileMap, this);
        }

        public void OnTileClicked(Tile tile)
        {
            if (currentSelection != default)
                tileMapView.HighlightTile(currentSelection, false);

            // simply deselect
            if (currentSelection == tile)
            {
                currentSelection = default;
            }
            // try matching
            else if (tileMap.MatchTiles(currentSelection, tile))
            {
                tileMapView.RemoveTileViews(currentSelection, tile);
                tileMap.RemoveTiles(currentSelection, tile);
                currentSelection = default;

                OnTilesMatched?.Invoke();

                if (tileMap.TilesCount <= 0)
                {
                    OnGameWon?.Invoke();
                    CloseButtonClickedHandler();
                }
            }
            else
            {
                // failed to match
                if (currentSelection != default)
                {
                    OnTilesMatchFailed?.Invoke();
                }

                currentSelection = tile;
                tileMapView.HighlightTile(tile, true);

            }
        }

        public void CloseButtonClickedHandler()
        {
            LevelLoader.Instance.LoadTitle();
        }

        //private void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.W))
        //    {
        //        OnGameWon?.Invoke();
        //    }
        //}
    }
}