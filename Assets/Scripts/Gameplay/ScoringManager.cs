﻿using Models;
using System.Collections.Generic;
using UI.Gameplay;
using UnityEngine;

namespace Gameplay
{
    public class ScoringManager : MonoBehaviour
    {
        [SerializeField]
        private ScoreView scoreView;

        [SerializeField]
        private GameplayManager gameplay;

        [Header("Settings")]
        [SerializeField]
        private int bonusScore;

        [SerializeField]
        private int penaltyScore;

        private int currentScore;
        private int highScore;

        private void Start()
        {
            scoreView.UpdateScore(currentScore);

            var highScores = PersistanceManager.Instance.LoadData<HighScores>();
            if (highScores.scores != null)
            {
                highScore = highScores.scores.Find(s => s.level == LevelLoader.Instance.LevelName).score;
            }
        }

        private void OnEnable()
        {
            gameplay.OnGameWon += GameWonHandler;

            gameplay.OnTilesMatched += TilesMatchedHandler;
            gameplay.OnTilesMatchFailed += TilesMatchFailedHandler;
        }

        private void OnDisable()
        {
            gameplay.OnGameWon -= GameWonHandler;

            gameplay.OnTilesMatched -= TilesMatchedHandler;
            gameplay.OnTilesMatchFailed -= TilesMatchFailedHandler;
        }

        private void GameWonHandler()
        {
            if (currentScore > highScore)
            {
                var highScores = PersistanceManager.Instance.LoadData<HighScores>();

                if (highScores.scores == null)
                    highScores.scores = new List<HighScore>();

                var currentLevel = LevelLoader.Instance.LevelName;
                var previousScore = highScores.scores.Find(s => s.level == currentLevel);
                if (!previousScore.Equals(default))
                    highScores.scores.Remove(previousScore);

                highScores.scores.Add(new HighScore { level = currentLevel, score = currentScore });
                PersistanceManager.Instance.SaveData(highScores);
            }
        }

        private void TilesMatchedHandler()
        {
            currentScore += bonusScore;

            scoreView.UpdateScore(currentScore);
        }

        private void TilesMatchFailedHandler()
        {
            currentScore -= penaltyScore;
            currentScore = currentScore < 0 ? 0 : currentScore;

            scoreView.UpdateScore(currentScore);
        }
    }
}