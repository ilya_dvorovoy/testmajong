﻿using Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using UI.Gameplay;
using UnityEngine;

namespace UI
{
    public class TileMapView : MonoBehaviour
    {
        [SerializeField]
        private TileViewConfiguration config;

        private TileMap tileMap;
        private GameplayManager gameplay;

        private Transform cachedTransform;

        private Dictionary<Tile, TileView> tileViews;

        public void Initialize(TileMap tileMap, GameplayManager gameplay)
        {
            this.tileMap = tileMap;
            this.gameplay = gameplay;

            tileViews = new Dictionary<Tile, TileView>();
        }

        private void Start()
        {
            cachedTransform = transform;
            var scale = CalculateTileScale(tileMap.Columns * config.TileWidth, tileMap.Rows * config.TileHeight);

            if (tileMap != null)
            {
                foreach (var tile in tileMap.Tiles)
                {
                    var tileView = config.CreateTile(tile, cachedTransform, gameplay.OnTileClicked);

                    if (scale < 1)
                    {
                        tileView.transform.localScale = new Vector3(scale, scale, 1);
                    }

                    var position = new Vector2(
                        scale * config.TileWidth * (tile.Coordinates.x - tileMap.Columns / 2f + 0.5f),
                        scale * config.TileHeight * (tileMap.Rows / 2f - tile.Coordinates.y - 0.5f)
                        );

                    tileView.transform.localPosition = position;

                    tileViews.Add(tile, tileView);
                }
            }
        }

        private float CalculateTileScale(float fieldWidth, float fieldHeight)
        {
            var canvas = GetComponentInParent<Canvas>();

            return Mathf.Min(Mathf.Clamp01(Screen.width / (fieldWidth * canvas.scaleFactor)), Mathf.Clamp01(Screen.height / (fieldHeight * canvas.scaleFactor)));
        }

        public void RemoveTileViews(params Tile[] tiles)
        {
            foreach (var tile in tiles)
            {
                var view = tileViews[tile];
                Destroy(view.gameObject);
            }
        }

        public void HighlightTile(Tile tile, bool enable)
        {
            tileViews[tile].Highlight(enable);
        }
    }
}