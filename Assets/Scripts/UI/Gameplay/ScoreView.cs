﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField]
        private Text scoreCounter;

        public void UpdateScore(int score)
        {
            scoreCounter.text = $"Score: {score}";
        }
    }
}