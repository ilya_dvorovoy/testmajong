﻿using Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class TileView : MonoBehaviour, IPointerClickHandler
    {
        private static readonly Color NORMAL = Color.white;
        private static readonly Color HIGHLIGHT = Color.yellow;

        [SerializeField]
        private Image background;

        [SerializeField]
        private Image face;

        private Tile tile;

        private Action<Tile> onClickHandler;

        public void Initialize(Tile tile, Sprite face, Action<Tile> onClickHandler)
        {
            this.tile = tile;
            this.face.sprite = face;
            this.onClickHandler = onClickHandler;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log($"Clicked {tile.Coordinates}");
            onClickHandler?.Invoke(tile);
        }

        internal void Highlight(bool enable)
        {
            background.color = enable ? HIGHLIGHT : NORMAL;
        }
    }
}