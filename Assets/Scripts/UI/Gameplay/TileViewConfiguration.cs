﻿using Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Gameplay
{
    [CreateAssetMenu(fileName = nameof(TileViewConfiguration), menuName = "Configurations/" + nameof(TileViewConfiguration))]
    public class TileViewConfiguration : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        private int tileWidth;
        public int TileWidth => tileWidth;

        [SerializeField]
        private int tileHeight;
        public int TileHeight => tileHeight;

        [SerializeField]
        private TileView tilePrefab;

        [SerializeField]
        private List<TileFace> tileFaces;

        private Dictionary<TileType, Sprite> tileTypes;

        public TileView CreateTile(Tile tile, Transform container, Action<Tile> onClickHandler)
        {
            var tileView = GameObject.Instantiate<TileView>(tilePrefab, container);
            tileView.Initialize(tile, tileTypes[tile.Type], onClickHandler);
            return tileView;
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            tileTypes = new Dictionary<TileType, Sprite>();
            foreach (var record in tileFaces)
            {
                tileTypes.Add(record.Type, record.Face);
            }
        }

        [Serializable]
        private struct TileFace
        {
            public TileType Type;
            public Sprite Face;
        }
    }
}