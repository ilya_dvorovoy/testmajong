﻿using Models;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Title
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField]
        private MenuView menuView;

        [SerializeField]
        private List<TextAsset> levels;

        private void Awake()
        {
            var highScores = PersistanceManager.Instance.LoadData<HighScores>();

            var levelNames = levels.ConvertAll(l => l.name).ToArray();
            var completedIndex = levels.ConvertAll(l => highScores.scores != null && highScores.scores.FindIndex(s => s.level == l.name) >= 0).ToArray();
            menuView.Initialize(levelNames, completedIndex, LevelSelectedHandler);
        }

        private void LevelSelectedHandler(int index)
        {
            LevelLoader.Instance.LoadLevel(levels[index]);
        }
    }
}