﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Title
{
    public class LevelSelectionButton : MonoBehaviour
    {
        [SerializeField]
        private Text label;

        [SerializeField]
        private GameObject completedContainer;

        private int levelIndex;

        private Action<int> onButtonClicked;

        public void Setup(string label, int levelIndex, bool completed, Action<int> onButtonClicked)
        {
            this.levelIndex = levelIndex;
            this.onButtonClicked = onButtonClicked;

            this.label.text = label;

            completedContainer.SetActive(completed);
        }

        public void ButtonClickedHandler()
        {
            onButtonClicked?.Invoke(levelIndex);
        }
    }
}