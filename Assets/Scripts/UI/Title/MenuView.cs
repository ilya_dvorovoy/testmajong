﻿using System;
using UnityEngine;

namespace UI.Title
{
    public class MenuView : MonoBehaviour
    {
        [SerializeField]
        private LevelSelectionButton buttonPrefab;

        public void Initialize(string[] levels, bool[] completed, Action<int> onLevelSelected)
        {
            for (int i = 0; i < levels.Length; i++)
            {
                var button = GameObject.Instantiate<LevelSelectionButton>(buttonPrefab, transform);
                button.Setup(levels[i], i, completed[i], onLevelSelected);
            }
        }
    }
}