﻿using UnityEngine;

public class PersistanceManager : Singleton<PersistanceManager>
{
    protected override void Awake()
    {
        base.Awake();

        DontDestroyOnLoad(this);
    }

    public void SaveData<T>(T data)
    {
        PlayerPrefs.SetString(typeof(T).ToString(), JsonUtility.ToJson(data));
        PlayerPrefs.Save();
    }

    public T LoadData<T>()
    {
        if (PlayerPrefs.HasKey(typeof(T).ToString()))
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(typeof(T).ToString()));
        }

        return default;
    }
}
