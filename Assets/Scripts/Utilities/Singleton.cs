﻿using UnityEngine;

/// <summary>
/// Generic Implementation of a Singleton MonoBehaviour.
/// </summary>
/// <typeparam name="T"></typeparam>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    /// <summary>
    /// Returns the instance of this singleton.
    /// </summary>
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                var instances = FindObjectsOfType(typeof(T));
                if (instances.Length > 1 || instances.Length <= 0)
                {
                    Debug.LogError("Zero or more than a single instance of " + typeof(T).ToString() + " found!");
                    return null;
                }

                instance = (T)instances[0];

                if (instance == null)
                {
                    Debug.LogError("An instance of " + typeof(T) + " is needed in the scene, but there is none.");
                }
            }

            return instance;
        }
    }

    protected static T instance;

    protected virtual void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
    }
}